<header class="header">
    <div class="wrap">
        <?php print render($page['header_left']); ?>
        <div class="header__logo">
            <?php if ($logo): ?>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><img
                            src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/></a>
            <?php endif; ?>
        </div>
        <?php print render($page['header_right']); ?>
        <?php print render($page['header_top']); ?>
    </div>
</header>

<div id="main">
    <div class="wrap">
        <?php print render($page['highlighted']); ?>
        <?php print $breadcrumb; ?>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
            <h1><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php print $messages; ?>
        <?php print render($tabs); ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>
    </div>
</div>


<?php print render($page['footer']); ?>

<?php print render($page['bottom']); ?>
