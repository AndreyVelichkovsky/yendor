(function ($, Drupal, window, document) {

    'use strict';

    Drupal.behaviors.search = {
        attach: function (context, settings) {

            $("#block-search-form .block__title", context).click(function () {
                $("#search-block-form").toggleClass("open");
            });

        }
    };

    Drupal.behaviors.colorLabelHide = {
        attach: function (context, settings) {

            $(".form-item-attributes-field-color label.option", context).remove();


            $(document).ajaxComplete(
                function () {

                    $(".form-item-attributes-field-color label.option", context).remove();

                }
            );

        }
    };


    $("#gal1").elevateZoom({
        gallery: 'gallery_01',
        cursor: 'pointer',
        galleryActiveClass: 'active',
        imageCrossfade: true,
        loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'
    });

    //pass the images to Fancybox
    $("#gal1").bind("click", function (e) {
        var ez = $('#zoom_03').data('elevateZoom');
        $.fancybox(ez.getGalleryList());
        return false;
    });

})(jQuery, Drupal, this, this.document);
